package main

import (
	"bufio"
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"path/filepath"
	"strings"
)

var ProjectDir string
var DatabaseDir string
var Debug bool

type Project struct {
	Name string `json:"name"`
}

func getEnv(key, fallback string) string {
	if value, ok := os.LookupEnv(key); ok {
		return value
	}
	return fallback
}

func printLog(namespace string, message string, params ...interface{}) {
	if Debug {
		fmt.Printf(fmt.Sprintf("[%s]", namespace)+message, params...)
		fmt.Print("\n")
	}
}

func writeData(name string, path string) {
	if name == "" {
		name = "_"
	}

	printLog("writeData", "write data: %s, path: %s", name, path)

	f, err := os.OpenFile(DatabaseDir, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	_, err = f.WriteString(fmt.Sprintf("%s %s \n", name, path))
	if err != nil {
		log.Fatal(err)
	}
	f.Sync()
}

func isJavascriptProject(path string) (bool, Project) {
	printLog("isJavascriptProject", "start check if %s is a js project", path)
	packageJSON := path + "/package.json"
	var project Project

	if _, err := os.Stat(packageJSON); !os.IsNotExist(err) {
		project = readPackageJSON(packageJSON)
		printLog("isJavascriptProject", "is js project")
		return true, project
	}

	printLog("isJavascriptProject", "not js project")
	return false, project
}

func readPackageJSON(path string) Project {
	printLog("readPackageJSON", "read package json at %s", path)
	jsonFile, err := os.Open(path)
	var project Project

	if err != nil {
		log.Fatal(err)
	}

	defer jsonFile.Close()
	byteValue, _ := ioutil.ReadAll(jsonFile)
	json.Unmarshal(byteValue, &project)

	return project
}

func buildDatabase() {
	/* Clear database */
	if _, err := os.Stat(DatabaseDir); !os.IsNotExist(err) {
		err2 := os.Remove(DatabaseDir)
		if err2 != nil {
			log.Fatal(err2)
		}
	}

	/* Clear database */
	err := filepath.Walk(ProjectDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}

		printLog("buildDatabase", "walking %s", path)

		/* ignore .git folder */
		if strings.Contains(path, ".git") {
			return filepath.SkipDir
		}

		/* ignore node_modules folder */
		if strings.Contains(path, "node_modules") {
			return filepath.SkipDir
		}

		/* not check file */
		if !info.IsDir() {
			return nil
		}

		if result, project := isJavascriptProject(path); result {
			writeData(project.Name, path)
			return filepath.SkipDir
		}

		return nil
	})

	if err != nil {
		log.Println(err)
	}
}

func ReadDatabase(action func(data string)) {
	f, err := os.Open(DatabaseDir)
	if err != nil {
		log.Fatal(err)
	}
	defer f.Close()

	scanner := bufio.NewScanner(f)

	for scanner.Scan() {
		data := scanner.Text()
		action(data)
	}

	if err := scanner.Err(); err != nil {
		log.Fatal(err)
	}
}

func searchProject(searchString string) {
	ReadDatabase(func(data string) {
		splits := strings.Split(data, " ")
		projectName := splits[0]
		path := splits[1]
		if strings.Contains(projectName, searchString) {
			fmt.Printf("%s %s\n", projectName, path)
		}
	})
}

func listAllProject() {
	ReadDatabase(func(data string) {
		fmt.Println(data)
	})
}

func main() {
	var searchString string
	var build bool
	var list bool
	ProjectDir = getEnv("PROJECT_DIR", "")
	DatabaseDir = getEnv("DATABASE_DIR", ".") + "/data"
	flag.BoolVar(&build, "build", false, "build project database")
	flag.StringVar(&searchString, "s", "", "search project")
	flag.BoolVar(&list, "l", false, "list project")
	flag.BoolVar(&Debug, "debug", false, "debug mode")
	flag.Parse()

	if ProjectDir == "" {
		fmt.Println("no PROJECT_DIR set exit")
		os.Exit(1)
	}

	if DatabaseDir == "./data" {
		fmt.Println("WARNING: DATABASE_DIR is not set, default to current directory")
	}

	if build {
		buildDatabase()
	}

	if list {
		listAllProject()
	}

	if searchString != "" {
		searchProject(searchString)
	}

}
